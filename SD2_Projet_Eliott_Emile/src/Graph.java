import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.File;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class Graph {
  private Map<Station, Set<Troncon>> tronconsSortants;
  private Map<String, Station> nomsStations;// pour referencer un nom a une station
  private int nbrTroncons;
  private int duree;

  public Graph() {
    this.tronconsSortants = new HashMap<Station, Set<Troncon>>();
    nomsStations = new HashMap<>();
    this.nbrTroncons = 0;
    this.duree = 0;
  }

  public void calculerCheminMinimisantNombreTroncons(String depart, String arrivee, String output) {
    Deque<Station> fileStations = new ArrayDeque<>();
    Set<Station> stationsDejaVisitees = new HashSet<>();
    Map<Station, Troncon> cheminParcourus = new HashMap<>();
    Station stationArr = nomsStations.get(arrivee);
    Station stationDep = nomsStations.get(depart);
    boolean flagArr = true;

    fileStations.addLast(stationDep);
    stationsDejaVisitees.add(stationDep);
    while (!fileStations.isEmpty() && flagArr) {
      Station currentStation = fileStations.removeFirst();
      Set<Troncon> troncons = arcsSortant(currentStation);
      for (Troncon troncon : troncons) {
        if (!stationsDejaVisitees.contains(troncon.getArrivee())) {
          cheminParcourus.put(troncon.getArrivee(), troncon);
          fileStations.add(troncon.getArrivee());
          stationsDejaVisitees.add(troncon.getArrivee());
        }
        if (troncon.getArrivee().equals(stationArr)) {
          flagArr = false;
          break;
        }
      }
    }

    if (!flagArr) {
      creerFichierXml(stationDep, stationArr, output, cheminParcourus);
    } else {
      throw new RuntimeException();
    }
  }

  public void calculerCheminMinimisantTempsTransport(String depart, String arrivee, String output) {
    Map<Station, Troncon> cheminParcourus = new HashMap<Station, Troncon>();
    TreeSet<Station> etiquettesProv = new TreeSet<Station>();
    Set<Station> etiquetteDef = new HashSet<Station>();
    Station stationArr = nomsStations.get(arrivee);
    Station stationDep = nomsStations.get(depart);
    stationDep.setDuree(0);
    etiquettesProv.add(stationDep);
    boolean flagArr = true;
    Station currentStation;
    while (!etiquetteDef.contains(stationArr)) {
      currentStation = etiquettesProv.pollFirst();
      etiquetteDef.add(currentStation);
      if (currentStation.equals(stationArr)) {
        flagArr = false;
        break;
      }
      Set<Troncon> troncons = arcsSortant(currentStation);
      for (Troncon troncon : troncons) {
        if (!etiquetteDef.contains(troncon.getArrivee())) {
          int duree = troncon.getDuree() + currentStation.getDuree();
          if (duree < troncon.getArrivee().getDuree()) {// 1. enlever du treeSet 2.setDuree 3.
                                                        // ajouter au
            etiquettesProv.remove(troncon.getArrivee()); // TreeSet
            troncon.getArrivee().setDuree(duree);
            etiquettesProv.add(troncon.getArrivee());
            cheminParcourus.put(troncon.getArrivee(), troncon);// modifier uniquement si on a
                                                               // trouv�s un
                                                               // meilleur chemin
          }
        }
      }
    }
    if (!flagArr) {
      creerFichierXml(stationDep, stationArr, output, cheminParcourus);
    } else {
      throw new RuntimeException();
    }
  }



  public void ajouterSommet(String nom, Station station) {
    tronconsSortants.put(station, new HashSet<>());
    nomsStations.put(nom, station);
  }

  public void ajouterArc(Troncon troncon) {
    tronconsSortants.get(troncon.getDepart()).add(troncon);
  }

  public Station getStationByNom(String s) {
    return nomsStations.get(s);
  }

  public String toString() {
    return tronconsSortants.toString();
  }

  private Set<Troncon> arcsSortant(Station s) {
    return tronconsSortants.get(s);
  }

  private Map<Station, Troncon> inverserCheminParcourus(Map<Station, Troncon> inverse,
      Station arrivee, Station depart) {
    Station tmp = arrivee;
    HashMap<Station, Troncon> bonSens = new HashMap<>();
    while (!tmp.equals(depart)) {
      this.nbrTroncons++;
      this.duree += inverse.get(tmp).getDuree() + inverse.get(tmp).getAttenteMoyenne();
      bonSens.put(inverse.get(tmp).getDepart(), inverse.get(tmp));
      tmp = inverse.get(tmp).getDepart();
    }
    return bonSens;
  }

  private void creerFichierXml(Station depart, Station arrivee, String outputNom,
      Map<Station, Troncon> inverse) {
    this.nbrTroncons = 0;
    this.duree = 0;
    HashMap<Station, Troncon> cheminParcourus =
        (HashMap<Station, Troncon>) inverserCheminParcourus(inverse, arrivee, depart);
    System.out.println(this.nbrTroncons);
    System.out.println(this.duree);
    System.out.println(cheminParcourus.get(depart).getArrivee().getNom());
    try {
      Station current = depart;
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.newDocument();
      Element rootElement = doc.createElement("trajet");
      doc.appendChild(rootElement);
      rootElement.setAttribute("depart", depart.getNom());
      rootElement.setAttribute("arrivee", arrivee.getNom());
      rootElement.setAttribute("duree", String.valueOf(duree));
      rootElement.setAttribute("nbrTroncons", String.valueOf(nbrTroncons));

      int nbTronconsLigne = 0;
      int duree = 0;
      String derniereLigne = cheminParcourus.get(current).getNom();
      String stDepart = current.getNom();

      Station derniere = null;

      while (!current.equals(arrivee)) {
        if (cheminParcourus.get(current).getNom().equals(derniereLigne)) {
          nbTronconsLigne++;
          duree += cheminParcourus.get(current).getDuree();
          derniere = current;
          current = cheminParcourus.get(current).getArrivee();
        } else {
          Element deplacement = doc.createElement("deplacement");
          rootElement.appendChild(deplacement);
          Attr attrArrivee = doc.createAttribute("arrivee");
          Attr attrAttenteMoy = doc.createAttribute("attenteMoyenne");
          Attr attrDepart = doc.createAttribute("depart");
          Attr attrDirection = doc.createAttribute("direction");
          Attr attrDuree = doc.createAttribute("duree");
          Attr attrNbTroncon = doc.createAttribute("nbTroncon");
          Attr attrType = doc.createAttribute("type");
          attrArrivee.setValue(cheminParcourus.get(derniere).getArrivee().getNom());
          System.out.println("test" + cheminParcourus.get(derniere).getArrivee().getNom());
          deplacement.setAttributeNode(attrArrivee);
          attrAttenteMoy
              .setValue(String.valueOf(cheminParcourus.get(derniere).getAttenteMoyenne()));
          deplacement.setAttributeNode(attrAttenteMoy);
          attrDepart.setValue(stDepart);
          deplacement.setAttributeNode(attrDepart);
          attrDirection.setValue(cheminParcourus.get(derniere).getDestination().getNom());
          deplacement.setAttributeNode(attrDirection);
          attrDuree.setValue(String.valueOf(duree));
          deplacement.setAttributeNode(attrDuree);
          attrNbTroncon.setValue(String.valueOf(nbTronconsLigne));
          deplacement.setAttributeNode(attrNbTroncon);
          attrType.setValue(cheminParcourus.get(derniere).getType());
          deplacement.setAttributeNode(attrType);
          deplacement.appendChild(doc.createTextNode(cheminParcourus.get(derniere).getNom()));
          current = cheminParcourus.get(derniere).getArrivee();
          derniereLigne = cheminParcourus.get(current).getNom();
          stDepart = current.getNom();
          nbTronconsLigne = 0;
          duree = 0;
        }
      }
      Element deplacement = doc.createElement("deplacement");
      rootElement.appendChild(deplacement);
      Attr attrArrivee = doc.createAttribute("arrivee");
      Attr attrAttenteMoy = doc.createAttribute("attenteMoyenne");
      Attr attrDepart = doc.createAttribute("depart");
      Attr attrDirection = doc.createAttribute("direction");
      Attr attrDuree = doc.createAttribute("duree");
      Attr attrNbTroncon = doc.createAttribute("nbTroncon");
      Attr attrType = doc.createAttribute("type");
      attrArrivee.setValue(cheminParcourus.get(derniere).getArrivee().getNom());
      System.out.println("test" + cheminParcourus.get(derniere).getArrivee().getNom());
      deplacement.setAttributeNode(attrArrivee);
      attrAttenteMoy.setValue(String.valueOf(cheminParcourus.get(derniere).getAttenteMoyenne()));
      deplacement.setAttributeNode(attrAttenteMoy);
      attrDepart.setValue(stDepart);
      deplacement.setAttributeNode(attrDepart);
      attrDirection.setValue(cheminParcourus.get(derniere).getDestination().getNom());
      deplacement.setAttributeNode(attrDirection);
      attrDuree.setValue(String.valueOf(duree));
      deplacement.setAttributeNode(attrDuree);
      attrNbTroncon.setValue(String.valueOf(nbTronconsLigne));
      deplacement.setAttributeNode(attrNbTroncon);
      attrType.setValue(cheminParcourus.get(derniere).getType());
      deplacement.setAttributeNode(attrType);
      deplacement.appendChild(doc.createTextNode(cheminParcourus.get(derniere).getNom()));
      // enregistrer dans un fichier
      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      Transformer transformer = transformerFactory.newTransformer();
      DOMSource source = new DOMSource(doc);
      StreamResult result = new StreamResult(new File(outputNom));
      transformer.transform(source, result);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


}
