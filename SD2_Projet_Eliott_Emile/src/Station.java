public class Station implements Comparable<Station> {

	private String nom;
	private int duree = Integer.MAX_VALUE;

	

	public Station(String nom) {
		this.nom = nom;
	}

	public String getNom() {
		return nom;
	}
	
	public void setDuree(int duree) {
		this.duree = duree;
	}

	public int getDuree() {
		return duree;
	}

	

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Station other = (Station) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}

	@Override
	public int compareTo(Station arg0) {
		if(this.duree - arg0.duree ==0) {
			return arg0.nom.compareTo(this.nom);
		}
		return   this.duree - arg0.duree;
	}
}
