
public class Troncon {
  private Station depart;
  private Station arrivee;
  private int duree;
  private int attenteMoyenne;
  private String type;
  private Station source;
  private Station destination;
  private String nom;

  public Troncon(Station depart, Station arrivee, int duree, int attenteMoyenne, String type,Station source,Station destination,String nom) {
    this.depart = depart;
    this.arrivee = arrivee;
    this.duree = duree;
    this.attenteMoyenne = attenteMoyenne;
    this.type = type;
    this.source=source;
    this.destination=destination;
    this.nom=nom;
    
  }

  public Station getDepart() {
    return depart;
  }

  public Station getArrivee() {
    return arrivee;
  }

  public int getDuree() {
    return duree;
  }

  public int getAttenteMoyenne() {
    return attenteMoyenne;
  }

  public String getType() {
    return type;
  }

public Station getSource() {
	return source;
}

public Station getDestination() {
	return destination;
}

public String getNom() {
	return nom;
}
}
