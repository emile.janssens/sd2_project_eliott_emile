
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.HashMap;
import java.util.Map;


public class SAXHandler extends DefaultHandler {
  private String nom;
  private String stop;
  private String type;
  private String attenteMoyenne;
  private String duree;
  private String depart;
  private String arrivee;
  private Station station;
  private String source;
  private String destination;
  private String nomLigne;
  private Map<String, Station> stopStation = new HashMap<String, Station>();
  private Graph graph = new Graph();
  private boolean bStop = false;
  private boolean bTel = false;

  @Override
  public void startElement(String uri, String name, String qName, Attributes attributs) {
    if (qName.equals("station")) {
      nom = attributs.getValue("nom");
      station = new Station(nom);
      graph.ajouterSommet(nom, station);
    } else if (qName.equals("stop")) {
      bStop = true;
    } else if (qName.equals("ligne")) {
      type = attributs.getValue("type");
      source = attributs.getValue("source");
      destination = attributs.getValue("destination");
      nomLigne = attributs.getValue("nom");
      attenteMoyenne = attributs.getValue("attenteMoyenne");
    } else if (qName.equals("troncon")) {
      duree = attributs.getValue("duree");
      depart = attributs.getValue("depart");
      arrivee = attributs.getValue("arrivee");
      graph.ajouterArc(new Troncon(stopStation.get(depart), stopStation.get(arrivee),
          Integer.parseInt(duree), Integer.parseInt(attenteMoyenne), type,
          graph.getStationByNom(source), graph.getStationByNom(destination), nomLigne));
    }
  }

  public void endElement(String uri, String localName, String qName) {
    if (qName.equals("station"))
      return;
  }

  public void characters(char ch[], int start, int length) {
    if (bStop) {
      stopStation.put(new String(ch, start, length), station);
      bStop = false;
    }
  }


  public Graph getGraph() {
    return graph;
  }
}
